package calculator;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.DecimalFormat;


public class Controller {

    private DecimalFormat format = new DecimalFormat("###.######");

    @FXML
    private Button basicOperations, equations, converter;

    @FXML
    private TextField first, second, xPower, nPower, xSqrt, nSqrt, xFactorial, aLinear, bLinear, aQuadratic, bQuadratic, cQuadratic,
            a1System, b1System, a2System, b2System, c1System, c2System, fromTextField, toTextField;

    @FXML
    private Label sign, basicResult, powerResult, sqrtResult, factorialResult, linearResult, quadraticResult, systemResult, messageLabel;

    @FXML
    private ChoiceBox<String> toChoiceBox, fromChoiceBox;

    public static final String DEC = "DEC";
    public static final String OCT = "OCT";
    public static final String BIN = "BIN";
    public static final String HEX = "HEX";


    public void basicOperationsButtonHandler() throws IOException {
        Stage appStage = (Stage) basicOperations.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("BasicMode.fxml"));
        appStage.getScene().setRoot(root);
        appStage.show();
    }

    public void equationsButtonHandler() throws IOException {
        Stage appStage = (Stage) equations.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("EquationsMode.fxml"));
        appStage.getScene().setRoot(root);
        appStage.show();
    }

    public void converterButtonHandler() throws IOException {
        Stage appStage = (Stage) converter.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("ConverterMode.fxml"));
        appStage.getScene().setRoot(root);
        appStage.show();
    }

    public void handlePlusButton() {
        sign.setText("+");
        if (isDouble(first.getText()) && isDouble(second.getText())) {
            double answer = Double.parseDouble(first.getText()) + Double.parseDouble(second.getText());
            String output = format.format(answer);
            basicResult.setText(output);
        } else
            basicResult.setText("ERROR");
    }

    public void handleMinusButton() {
        sign.setText("-");
        if (isDouble(first.getText()) && isDouble(second.getText())) {
            double answer = Double.parseDouble(first.getText()) - Double.parseDouble(second.getText());
            String output = format.format(answer);
            basicResult.setText(output);
        } else
            basicResult.setText("ERROR");
    }

    public void handleMultiplyButton() {
        sign.setText("x");
        if (isDouble(first.getText()) && isDouble(second.getText())) {
            double answer = Double.parseDouble(first.getText()) * Double.parseDouble(second.getText());
            String output = format.format(answer);

            basicResult.setText(output);
        } else
            basicResult.setText("ERROR");
    }

    public void handleDivideButton() {
        sign.setText("/");
        if (isDouble(first.getText()) && isDouble(second.getText()) && Double.parseDouble(second.getText()) != 0) {
            double answer = Double.parseDouble(first.getText()) / Double.parseDouble(second.getText());
            String output = format.format(answer);

            basicResult.setText(output);
        } else
            basicResult.setText("ERROR");

    }

    public void handlePowerButton() {
        if (isDouble(xPower.getText()) && isDouble(nPower.getText())) {
            double answer = Math.pow(Double.parseDouble(xPower.getText()), Double.parseDouble(nPower.getText()));
            String output = format.format(answer);
            powerResult.setText(output);
        } else
            powerResult.setText("ERROR");
    }

    public void handleSqrtButton() {
        if (isDouble(xSqrt.getText()) && isDouble(nSqrt.getText())) {
            double answer = Math.pow(Double.parseDouble(xSqrt.getText()), 1 / Double.parseDouble(nSqrt.getText()));
            String output = format.format(answer);
            sqrtResult.setText(output);
        } else
            sqrtResult.setText("ERROR");
    }

    public void handleFactorialButton() {
        if (isDouble(xFactorial.getText())) {
            double fact = countFactorial(Double.parseDouble(xFactorial.getText()));
            String output = format.format(fact);
            factorialResult.setText(output);
        } else
            factorialResult.setText("ERROR");
    }

    public void handleCountLinearEquationButton() {
        if (isDouble(aLinear.getText()) && isDouble(bLinear.getText())) {
            double a = Double.parseDouble(aLinear.getText());
            double b = Double.parseDouble(bLinear.getText());
            if (a == 0) {
                if (b == 0) {
                    linearResult.setText("0");
                } else {
                    linearResult.setText("\u2205");
                }
            } else {
                double x = countLinearEquation(a, b);
                String output = format.format(x);
                linearResult.setText(output);
            }
        } else {
            linearResult.setText("ERROR");
        }
    }

    public void handleCountQuadraticEquationButton() {
        if (isDouble(aQuadratic.getText()) && isDouble(bQuadratic.getText()) && isDouble(cQuadratic.getText())) {

            double a = Double.parseDouble(aQuadratic.getText());
            double b = Double.parseDouble(bQuadratic.getText());
            double c = Double.parseDouble(cQuadratic.getText());

            if (a == 0)
                countLinearEquation(a, b);
            else {
                double deltaSqrt = Math.sqrt(countDelta(a, b, c));
                if (deltaSqrt > 0) {
                    double x1 = countX1(deltaSqrt, b, a);
                    String output1 = format.format(x1);
                    double x2 = countX2(deltaSqrt, b, a);
                    String output2 = format.format(x2);
                    quadraticResult.setText("x\u2081 = " + output1 + ", x\u2082 = " + output2);
                } else if (deltaSqrt == 0) {
                    double x0 = countLinearEquation(2 * a, b);
                    String output0 = format.format(x0);
                    quadraticResult.setText(" x\u2080  = " + output0);
                } else {
                    quadraticResult.setText("x = \u2205");
                }
            }

        } else
            quadraticResult.setText("ERROR");
    }

    public void handleCountSystemOfEquationsButton() {
        if (isDouble(a1System.getText()) && isDouble(a2System.getText()) && isDouble(b1System.getText()) && isDouble(b2System.getText()) && isDouble(c1System.getText()) && isDouble(c2System.getText())) {
            double a1 = Double.parseDouble(a1System.getText());
            double a2 = Double.parseDouble(a2System.getText());
            double b1 = Double.parseDouble(b1System.getText());
            double b2 = Double.parseDouble(b2System.getText());
            double c1 = Double.parseDouble(c1System.getText());
            double c2 = Double.parseDouble(c2System.getText());

            double W = a1 * b2 - b1 * a2;
            double Wx = c1 * b2 - b1 * c2;
            double Wy = a1 * c2 - c1 * a2;

            if (W == 0) {
                if (Wx == 0 && Wy == 0) {
                    systemResult.setText("Indeterminate");
                } else
                    systemResult.setText("Contrary");
            } else {
                double x = Wx / W;
                String output1 = format.format(x);
                double y = Wy / W;
                String output2 = format.format(y);
                systemResult.setText("x = " + output1 + '\n' + "y = " + output2);
            }


        }
    }


    public void handleConvertButton() {
        messageLabel.setText("");

        String from = fromChoiceBox.getValue();
        String to = toChoiceBox.getValue();
        String number = fromTextField.getText();
        String result;

        if (fromChoiceBox.getValue() == null || toChoiceBox.getValue() == null)
            return;


        if (from.equals(to))
            result = number;


        else if (from.equals(DEC))

            result = convertFromDec(number, to);

        else if (toChoiceBox.getValue().equals(DEC))
            result = convertToDec(number, from);

        else
            result = convertFromDec(convertToDec(number, from), to);

        toTextField.setText(result);


    }


    private String convertToDec(String number, String from) {

        switch (from){
            case BIN:
                return Integer.toString(Integer.parseInt(number,2));
            case OCT:
                return Integer.toString(Integer.parseInt(number,8));
            case HEX:
                return Integer.toString(Integer.parseInt(number,16));
            default:
                return "0";
        }
    }

    private String convertFromDec(String number, String to) {
        switch (to) {
            case BIN:
                return Integer.toBinaryString(Integer.parseInt(number));

            case OCT:
                return Integer.toOctalString(Integer.parseInt(number));

            case HEX:
                return Integer.toHexString(Integer.parseInt(number));

            default:
                return "0";
        }

    }


    private double countX2(double deltaSqrt, double b, double a) {
        return (-b + deltaSqrt) / 2 * a;
    }

    private double countX1(double deltaSqrt, double b, double a) {
        return (-b - deltaSqrt) / 2 * a;
    }

    private double countDelta(double a, double b, double c) {
        return Math.pow(b, 2) - 4 * a * c;
    }

    private double countLinearEquation(double a, double b) {
        return -b / a;
    }

    private boolean isDouble(String data) {
        try {
            Double.parseDouble(data);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }


    private double countFactorial(double number) {
        if (number == 0 || number == 1)
            return 1;
        double result = 1;
        for (int i = 2; i <= number; i++) {
            result *= i;
        }
        return result;
    }
}


